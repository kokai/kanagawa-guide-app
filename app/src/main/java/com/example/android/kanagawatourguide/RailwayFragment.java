package com.example.android.kanagawatourguide;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class RailwayFragment extends Fragment {

    // URL link to the line information
    private static final String URL_LINE_SHINKANSEN = "http://ekikara.jp/newdata/line/2301011.htm";
    private static final String URL_LINE_KEIHINTOHOKU = "http://ekikara.jp/newdata/line/1301101.htm";
    private static final String URL_LINE_TOKAIDO = "http://ekikara.jp/newdata/line/1301131.htm";
    private static final String URL_LINE_NAMBU = "http://ekikara.jp/newdata/line/1301351.htm";
    private static final String URL_LINE_YOKOSUKA = "http://ekikara.jp/newdata/line/1301291.htm";
    private static final String URL_LINE_YOKOHAMA = "http://ekikara.jp/newdata/line/1301121.htm";
    private static final String URL_LINE_ODAWARA = "http://ekikara.jp/newdata/line/1306011.htm";
    private static final String URL_LINE_ODAKYU_ENOSHIMA = "http://ekikara.jp/newdata/line/1306021.htm";
    private static final String URL_LINE_TAMA = "http://ekikara.jp/newdata/line/1306031.htm";
    private static final String URL_LINE_TOYOKO = "http://ekikara.jp/newdata/line/1307011.htm";
    private static final String URL_LINE_TENENTOSHI = "http://ekikara.jp/newdata/line/1307051.htm";
    private static final String URL_LINE_MEGURO = "http://ekikara.jp/newdata/line/1307021.htm";
    private static final String URL_LINE_SAGAMIHARA = "http://ekikara.jp/newdata/line/1305021.htm";
    private static final String URL_LINE_YOKOHAMA_BLUE = "http://ekikara.jp/newdata/line/1403011.htm";
    private static final String URL_LINE_YOKOHAMA_GREEN = "http://ekikara.jp/newdata/line/1403021.htm";
    private static final String URL_LINE_ENOSHIMA = "http://ekikara.jp/newdata/line/1406011.htm";

    public RailwayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.spot_list, container, false);

        // create railway list
        ArrayList<SpotItem> rails = new ArrayList<>();
        rails.add(new SpotItem(R.string.line_shinkansen, R.string.jr_tokai, URL_LINE_SHINKANSEN));
        rails.add(new SpotItem(R.string.line_keihintohoku, R.string.jr_east, URL_LINE_KEIHINTOHOKU));
        rails.add(new SpotItem(R.string.line_tokaido, R.string.jr_east, URL_LINE_TOKAIDO));
        rails.add(new SpotItem(R.string.line_nambu, R.string.jr_east, URL_LINE_NAMBU));
        rails.add(new SpotItem(R.string.line_yokosuka, R.string.jr_east, URL_LINE_YOKOSUKA));
        rails.add(new SpotItem(R.string.line_yokohama, R.string.jr_east, URL_LINE_YOKOHAMA));
        rails.add(new SpotItem(R.string.line_odawara, R.string.odakyu_company, URL_LINE_ODAWARA));
        rails.add(new SpotItem(R.string.line_odakyuenoshima, R.string.odakyu_company, URL_LINE_ODAKYU_ENOSHIMA));
        rails.add(new SpotItem(R.string.line_tama, R.string.odakyu_company, URL_LINE_TAMA));
        rails.add(new SpotItem(R.string.line_toyoko, R.string.tokyu_company, URL_LINE_TOYOKO));
        rails.add(new SpotItem(R.string.line_tenentoshi, R.string.tokyu_company, URL_LINE_TENENTOSHI));
        rails.add(new SpotItem(R.string.line_meguro, R.string.tokyu_company, URL_LINE_MEGURO));
        rails.add(new SpotItem(R.string.line_sagamihara, R.string.keio_company, URL_LINE_SAGAMIHARA));
        rails.add(new SpotItem(R.string.line_yokohama_blue, R.string.yokohama_subway, URL_LINE_YOKOHAMA_BLUE));
        rails.add(new SpotItem(R.string.line_yokohama_green, R.string.yokohama_subway, URL_LINE_YOKOHAMA_GREEN));
        rails.add(new SpotItem(R.string.line_enoshima, R.string.enoshima_company, URL_LINE_ENOSHIMA));

        // create the list view adapter for the mountains list
        final SpotItemAdapter adapter = new SpotItemAdapter(getActivity(), rails, R.color.category_railways);

        ListView listView = (ListView) rootView.findViewById(R.id.spot_list_view);

        listView.setAdapter(adapter);

        // set click listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Find the current railway that was clicked on
                SpotItem current = adapter.getItem(i);
                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri uri = Uri.parse(current.getUrl());
                // Create a new intent to view the URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, uri);
                // Send the intent to launch a new activity
                startActivity(websiteIntent);
            }
        });

        return rootView;
    }

}
