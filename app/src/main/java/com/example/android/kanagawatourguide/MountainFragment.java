package com.example.android.kanagawatourguide;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MountainFragment extends Fragment {


    public MountainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.spot_list, container, false);

        // create the mountains list
        ArrayList<SpotItem> mountains = new ArrayList<>();
        mountains.add(new SpotItem(R.string.mt_ishidana, R.drawable.mt_ishidana, R.string.mt_ishidana_height));
        mountains.add(new SpotItem(R.string.mt_usugatake, R.drawable.mt_usugatake, R.string.mt_usugatake_height));
        mountains.add(new SpotItem(R.string.mt_okoge, R.drawable.mt_okoge, R.string.mt_okoge_height));
        mountains.add(new SpotItem(R.string.mt_oyama, R.drawable.mt_oyama, R.string.mt_oyama_height));
        mountains.add(new SpotItem(R.string.mt_kami, R.drawable.mt_kamiyama, R.string.mt_kami_height));
        mountains.add(new SpotItem(R.string.mt_kanmurigatake, R.drawable.mt_kanmurigatake, R.string.mt_kanmurigatake_height));
        mountains.add(new SpotItem(R.string.mt_kintoki, R.drawable.mt_kintokiyama, R.string.mt_kintoki_height));
        mountains.add(new SpotItem(R.string.mt_sannoto, R.drawable.mt_sannotoh, R.string.mt_sannoto_height));
        mountains.add(new SpotItem(R.string.mt_sodehira, R.drawable.mt_sodehira, R.string.mt_sodehira_height));
        mountains.add(new SpotItem(R.string.mt_takatori, R.drawable.mt_takatori, R.string.mt_takatori_height));
        mountains.add(new SpotItem(R.string.mt_nabewari, R.drawable.mt_nabewari, R.string.mt_nabewari_height));
        mountains.add(new SpotItem(R.string.mt_hanadate, R.drawable.mt_hanadate, R.string.mt_hanadate_height));
        mountains.add(new SpotItem(R.string.mt_hannokimaru, R.drawable.mt_hannokimaru, R.string.mt_hannokimaru_height));
        mountains.add(new SpotItem(R.string.mt_harutake, R.drawable.mt_harutakeyama, R.string.mt_harutake_height));
        mountains.add(new SpotItem(R.string.mt_hirugatake, R.drawable.mt_hirugatake, R.string.mt_hirugatake_height));
        mountains.add(new SpotItem(R.string.mt_fudonomine, R.drawable.mt_fudohnomine, R.string.mt_fudonomine_height));
        mountains.add(new SpotItem(R.string.mt_yaguradake, R.drawable.mt_yaguradake, R.string.mt_yaguradake_height));
        mountains.add(new SpotItem(R.string.mt_ryugabanba, R.drawable.mt_ryugabamba, R.string.mt_ryugabanba_height));

        // create the list view adapter for the mountains list
        SpotItemAdapter adapter = new SpotItemAdapter(getActivity(), mountains, R.color.category_mountains);

        ListView listView = (ListView) rootView.findViewById(R.id.spot_list_view);

        listView.setAdapter(adapter);

        return rootView;
    }

}
