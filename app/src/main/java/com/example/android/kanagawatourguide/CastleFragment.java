package com.example.android.kanagawatourguide;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CastleFragment extends Fragment {

    // URL link of the introduction web site of the castles
    private static final String URL_CASTLE_AOKI = "http://www.asahi-net.or.jp/~qb2t-nkns/musasiaoki.htm";
    private static final String URL_CASTLE_GONGENYAMA = "http://www.asahi-net.or.jp/~qb2t-nkns/gongenyama.htm";
    private static final String URL_CASTLE_KODUKUE = "http://www.asahi-net.or.jp/~qb2t-nkns/kodukue.htm";
    private static final String URL_CASTLE_CHIGASAKI = "http://www.asahi-net.or.jp/~qb2t-nkns/tigasaki.htm";
    private static final String URL_CASTLE_ENOSHITA = "http://www.asahi-net.or.jp/~qb2t-nkns/enosita.htm";
    private static final String URL_CASTLE_TSUKUI = "http://www.asahi-net.or.jp/~qb2t-nkns/tukui.htm";
    private static final String URL_CASTLE_ISHIGAKIYAMA = "http://www.asahi-net.or.jp/~qb2t-nkns/isigakiyama.htm";

    public CastleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.spot_list, container, false);

        // create a list of the castles
        final ArrayList<SpotItem> castles = new ArrayList<>();
        castles.add(new SpotItem(R.string.castle_musasiaoki, URL_CASTLE_AOKI, R.drawable.c_musasiaoki));
        castles.add(new SpotItem(R.string.castle_gongenyama, URL_CASTLE_GONGENYAMA, R.drawable.c_gongenyama));
        castles.add(new SpotItem(R.string.castle_kodekue, URL_CASTLE_KODUKUE, R.drawable.c_kodukue));
        castles.add(new SpotItem(R.string.castle_chigasaki, URL_CASTLE_CHIGASAKI, R.drawable.c_tigasaki));
        castles.add(new SpotItem(R.string.castle_enoshita, URL_CASTLE_ENOSHITA, R.drawable.c_enosita));
        castles.add(new SpotItem(R.string.castle_sagamitsukui, URL_CASTLE_TSUKUI, R.drawable.c_tukui));
        castles.add(new SpotItem(R.string.castle_ishigakiyama, URL_CASTLE_ISHIGAKIYAMA, R.drawable.c_isigakiyama));

        // create the spot adapter for castles list
        final SpotItemAdapter adapter = new SpotItemAdapter(getActivity(), castles, R.color.category_castles);

        ListView listView = (ListView) rootView.findViewById(R.id.spot_list_view);

        listView.setAdapter(adapter);

        // set click listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Find the current city that was clicked on
                SpotItem currentCastle = adapter.getItem(i);
                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri uri = Uri.parse(currentCastle.getUrl());
                // Create a new intent to view the wiki URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, uri);
                // Send the intent to launch a new activity
                startActivity(websiteIntent);
            }
        });

        return rootView;
    }
}
