package com.example.android.kanagawatourguide;

/**
 * The spot item for the tours guide
 */

public class SpotItem {

    private static final int NO_DESCRIPTION = -1;
    private static final int NO_IMAGE = -1;
    private static final String NO_LINK = "";

    private int mNameResource;
    private int mDescriptionResource = NO_DESCRIPTION;
    private String mUrl = NO_LINK;
    private int mImageResourceId = NO_IMAGE;

    /**
     * Create a new SpotItem object
     * @param name is the name of the spot item
     * @param desc is the descriptions of the spot
     * @param url is the link to the spot website
     * @param imageId is the image resource id of the spot
     */
    public SpotItem(int name, int desc, String url, int imageId) {
        mNameResource = name;
        mDescriptionResource = desc;
        mUrl = url;
        mImageResourceId = imageId;
    }

    /**
     * Create a new SpotItem object
     * @param name is the name of the spot item
     * @param url is the link to the spot website
     * @param imageId is the image resource id of the spot
     */
    public SpotItem(int name, String url, int imageId) {
        mNameResource = name;
        mUrl = url;
        mImageResourceId = imageId;
    }

    /**
     * Create a new SpotItem object without image resource
     * @param name is the name of the spot item
     * @param desc is the descriptions of the spot
     * @param url is the link to the spot website
     */
    public SpotItem(int name, int desc, String url) {
        mNameResource = name;
        mDescriptionResource = desc;
        mUrl = url;
    }

    /**
     * Create a new SpotItem object
     * @param name is the name of the spot item
     * @param desc is the descriptions of the spot
     * @param imageId is the image resource id of the spot
     */
    public SpotItem(int name, int imageId, int desc) {
        mNameResource = name;
        mDescriptionResource = desc;
        mImageResourceId = imageId;
    }


    /**
     * @return the name of the spot
     */
    public int getNameResource() {

        return mNameResource;
    }

    /**
     * @return the description of the spot
     */
    public int getDescriptionResource() {

        return mDescriptionResource;
    }

    /**
     * @return the image resource id for this spot
     */
    public int getImageResourceId() {

        return mImageResourceId;
    }

    /**
     * @return the url link for this spot
     */
    public String getUrl() {

        return mUrl;
    }

    /**
     * @return whether or not there is an image for this spot
     */
    public boolean hasImage() {

        return mImageResourceId != NO_IMAGE;
    }

    /**
     * @return whether or not there is an url link for this spot
     */
    public boolean hasUrlLink() {

        return !(mUrl.equals(NO_LINK));
    }

    /**
     * @return whether or not there is a description for this spot
     */
    public boolean hasDescription() {

        return mDescriptionResource != NO_DESCRIPTION;
    }
}
