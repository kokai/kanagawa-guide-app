package com.example.android.kanagawatourguide;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass that display a list of cities
 */
public class CityFragment extends Fragment {

    // URL string for all cities in the list
    private static final String URL_YOKOHAMA = "https://en.wikipedia.org/wiki/Yokohama";
    private static final String URL_KAWASAKI = "https://en.wikipedia.org/wiki/Kawasaki,_Kanagawa";
    private static final String URL_SAGAMIHARA = "https://en.wikipedia.org/wiki/Sagamihara";
    private static final String URL_YOKOSUKA = "https://en.wikipedia.org/wiki/Yokosuka,_Kanagawa";
    private static final String URL_HIRATSUKA = "https://en.wikipedia.org/wiki/Hiratsuka,_Kanagawa";
    private static final String URL_KAMAKURA = "https://en.wikipedia.org/wiki/Kamakura";
    private static final String URL_FUJISAWA = "https://en.wikipedia.org/wiki/Fujisawa,_Kanagawa";
    private static final String URL_ODAWARA = "https://en.wikipedia.org/wiki/Odawara";
    private static final String URL_CHIGASAKI = "https://en.wikipedia.org/wiki/Chigasaki,_Kanagawa";
    private static final String URL_ZUSHI = "https://en.wikipedia.org/wiki/Zushi,_Kanagawa";
    private static final String URL_MIURA = "https://en.wikipedia.org/wiki/Miura,_Kanagawa";
    private static final String URL_HADANO = "https://en.wikipedia.org/wiki/Hadano,_Kanagawa";
    private static final String URL_ATSUGI = "https://en.wikipedia.org/wiki/Atsugi,_Kanagawa";
    private static final String URL_YAMATO = "https://en.wikipedia.org/wiki/Yamato,_Kanagawa";
    private static final String URL_ISEHARA = "https://en.wikipedia.org/wiki/Isehara,_Kanagawa";
    private static final String URL_EBINA = "https://en.wikipedia.org/wiki/Ebina,_Kanagawa";
    private static final String URL_ZAMA = "https://en.wikipedia.org/wiki/Zama,_Kanagawa";
    private static final String URL_MINAMIASHIGARA = "https://en.wikipedia.org/wiki/Minamiashigara,_Kanagawa";
    private static final String URL_AYASE = "https://en.wikipedia.org/wiki/Ayase,_Kanagawa";


    public CityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.spot_list, container, false);

        // create a list of cities
        final ArrayList<SpotItem> cities = new ArrayList<>();
        cities.add(new SpotItem(R.string.city_yokohama, R.string.city_yokohama_detail, URL_YOKOHAMA, R.drawable.s_yokohama));
        cities.add(new SpotItem(R.string.city_kawasaki, R.string.city_kawasaki_detail, URL_KAWASAKI, R.drawable.s_kawasaki));
        cities.add(new SpotItem(R.string.city_sagamihara, R.string.city_sagamihara_detail, URL_SAGAMIHARA, R.drawable.s_sagami));
        cities.add(new SpotItem(R.string.city_yokosuka, R.string.city_yokosuka_detail, URL_YOKOSUKA, R.drawable.s_yokosuga));
        cities.add(new SpotItem(R.string.city_hiratsuka, R.string.city_hiratsuka_detail, URL_HIRATSUKA, R.drawable.s_hiratsuka));
        cities.add(new SpotItem(R.string.city_kamakura, R.string.city_kamakura_detail, URL_KAMAKURA, R.drawable.s_kamakura));
        cities.add(new SpotItem(R.string.city_fujisawa, R.string.city_fujisawa_detail, URL_FUJISAWA, R.drawable.s_fujisawa));
        cities.add(new SpotItem(R.string.city_odawara, R.string.city_odawara_detail, URL_ODAWARA, R.drawable.s_odawara));
        cities.add(new SpotItem(R.string.city_chigasaki, R.string.city_chigasaki_detail, URL_CHIGASAKI, R.drawable.s_chigasaki));
        cities.add(new SpotItem(R.string.city_zushi, R.string.city_zushi_detail, URL_ZUSHI, R.drawable.s_zushi));
        cities.add(new SpotItem(R.string.city_miura, R.string.city_miura_detail, URL_MIURA, R.drawable.s_miura));
        cities.add(new SpotItem(R.string.city_hadano, R.string.city_hadano_detail, URL_HADANO, R.drawable.s_hatano));
        cities.add(new SpotItem(R.string.city_atsugi, R.string.city_atsugi_detail, URL_ATSUGI, R.drawable.s_atsugi));
        cities.add(new SpotItem(R.string.city_yamato, R.string.city_yamato_detail, URL_YAMATO, R.drawable.s_yamato));
        cities.add(new SpotItem(R.string.city_isehara, R.string.city_isehara_detail, URL_ISEHARA, R.drawable.s_isehara));
        cities.add(new SpotItem(R.string.city_ebina, R.string.city_ebina_detail, URL_EBINA, R.drawable.s_ebina));
        cities.add(new SpotItem(R.string.city_zama, R.string.city_zama_detail, URL_ZAMA, R.drawable.s_zama));
        cities.add(new SpotItem(R.string.city_minamiashigara, R.string.city_minamiashigara_detail, URL_MINAMIASHIGARA, R.drawable.s_minamiashigara));
        cities.add(new SpotItem(R.string.city_ayase, R.string.city_ayase_detail, URL_AYASE, R.drawable.s_ayase));

        // Create an {@link SpotItemAdapter}, whose data source is a list of {@link SpotItem}s. The
        // adapter knows how to create list items for each item in the list.
        final SpotItemAdapter adapter = new SpotItemAdapter(getActivity(), cities, R.color.category_cities);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // spot_list.xml layout file.
        ListView listView = (ListView) rootView.findViewById(R.id.spot_list_view);
        // Make the {@link ListView} use the {@link SpotItemAdapter} we created above, so that the
        // {@link ListView} will display list items for each {@link SpotItem} in the list.
        listView.setAdapter(adapter);

        // set a click event handler
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Find the current city that was clicked on
                SpotItem currentCity = adapter.getItem(i);
                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri wikiUri = Uri.parse(currentCity.getUrl());
                // Create a new intent to view the wiki URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, wikiUri);
                // Send the intent to launch a new activity
                startActivity(websiteIntent);
            }
        });

        return rootView;
    }

}
