package com.example.android.kanagawatourguide;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * List view adapter class for the spots list
 */

public class SpotItemAdapter extends ArrayAdapter<SpotItem> {

    // resource id for the background for this list of spot
    private int mColorResourceId;

    public SpotItemAdapter(Context context, ArrayList<SpotItem> spots) {
        super(context, 0, spots);

        mColorResourceId = -1;
    }

    public SpotItemAdapter(Context context, ArrayList<SpotItem> spots, int colorId) {
        super(context, 0, spots);

        mColorResourceId = colorId;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // return super.getView(position, convertView, parent);

        // check if an existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView= LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        // get the {@link SpotItem} object located at this position in the list
        SpotItem currentSpot = getItem(position);
        // spot name text view
        TextView spotName = (TextView) listItemView.findViewById(R.id.text_view_spot_name);
        spotName.setText(currentSpot.getNameResource());
        // spot description text view
        TextView spotDescription = (TextView) listItemView.findViewById(R.id.text_view_spot_description);
        if (currentSpot.hasDescription())
        {
            spotDescription.setText(currentSpot.getDescriptionResource());
            spotDescription.setVisibility(View.VISIBLE);
        } else {
            spotDescription.setVisibility(View.GONE);
        }

        // spot image
        ImageView spotImage = (ImageView) listItemView.findViewById(R.id.image_view);
        if (currentSpot.hasImage()) {
            // If an image is available, display the provided image based on the resource ID
            spotImage.setImageResource(currentSpot.getImageResourceId());
            // make sure the view is visible
            spotImage.setVisibility(View.VISIBLE);
        } else {
            // Otherwise hide the ImageView (set visibility to GONE)
            spotImage.setVisibility(View.GONE);
        }


        // Set the theme color for the list item
        View textContainer = listItemView.findViewById(R.id.spot_item_container_view);
        // Find the color that the resource ID maps to
        int color = ContextCompat.getColor(getContext(), mColorResourceId);
        // Set the background color of the text container View
        textContainer.setBackgroundColor(color);


        // Return the whole list item layout (containing 2 TextViews) so that it can be shown in
        // the ListView.
        return listItemView;
    }

    /**
     * @return the color resource id
     */
    public int getColorResourceId() {

        return mColorResourceId;
    }
}
