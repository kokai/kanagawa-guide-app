package com.example.android.kanagawatourguide;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * {@link CategoryAdapter} is a {@link FragmentPagerAdapter} that can provide the layout for
 * each list item based on a data source which is a list of {@link SpotItem} objects.
 */
public class CategoryAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public CategoryAdapter(Context context, FragmentManager fm) {
        super(fm);

        mContext = context;
    }

    @Override
    public int getCount() {

        return 4;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CityFragment();
            case 1:
                return new CastleFragment();
            case 2:
                return new MountainFragment();
            case 3:
                return new RailwayFragment();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.tab_city);
            case 1:
                return mContext.getString(R.string.tab_castle);
            case 2:
                return mContext.getString(R.string.tab_mountain);
            case 3:
                return mContext.getString(R.string.tab_railway);
            default:
                return null;
        }
    }
}
