package com.example.android.kanagawatourguide;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // use activity_main.xml to set the content of the activity layout
        setContentView(R.layout.activity_main);

        // get the view pager to swipe between fragments
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        // create the fragment adaptor which controls which page should be shown
        CategoryAdapter adapter = new CategoryAdapter(this, getSupportFragmentManager());
        // set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // get the tab layout to show the tabs of each category
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        // connect the tab layout with the view pager
        tabLayout.setupWithViewPager(viewPager);
    }
}
